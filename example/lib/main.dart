import 'dart:io';

import 'package:dub_youtube_player/dub_youtube_player.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context2) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Temp(),
    );
  }
}

class Temp extends StatefulWidget {
  const Temp({Key? key}) : super(key: key);

  @override
  _TempState createState() => _TempState();
}

class _TempState extends State<Temp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: RaisedButton(onPressed: () {
      Navigator.push(context, MaterialPageRoute(builder: (ctx) => YoutubePlayerDemo()));
    })));
  }
}

class YoutubePlayerDemo extends StatefulWidget {
  const YoutubePlayerDemo({Key? key}) : super(key: key);

  @override
  State<YoutubePlayerDemo> createState() => _YoutubePlayerDemoState();
}

class _YoutubePlayerDemoState extends State<YoutubePlayerDemo> with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: OriginYoutubePlayer(startPoint: 1300),
              ),
              SliverToBoxAdapter(
                child: SizedBox(height: 3000),
              )
            ],
            // child: Column(
            //   mainAxisAlignment: MainAxisAlignment.start,
            //   children: <Widget>[
            //     DubYoutubePlayer(
            //       controller: _controller!,
            //       aspectRatio: 16 / 9,
            //     ),
            //     YoutubeValueBuilder(
            //       builder: (BuildContext, YoutubePlayerValue) {
            //         return Column(
            //           children: [
            //             Text('title : ${YoutubePlayerValue.metaData.title}'),
            //             Text('videoId : ${YoutubePlayerValue.metaData.videoId}'),
            //             Text('current pos : ${YoutubePlayerValue.position}'),
            //             Text('total pos : ${YoutubePlayerValue.metaData.duration}'),
            //             Text('buffered pos : ${YoutubePlayerValue.buffered}'),
            //           ],
            //         );
            //       },
            //     ),
            //     StreamBuilder<PlayerState>(
            //         stream: _controller!.playerStateStream,
            //         builder: (ctx, snap) {
            //           if (snap.hasData) {
            //             PlayerState state = snap.data as PlayerState;
            //             return Text('current state : ${state}');
            //           } else {
            //             return Text('current state : empty');
            //           }
            //         }),
            //     SizedBox(height: 3000),
            //     RaisedButton(
            //       child: Text('seekTo'),
            //       onPressed: () async {
            //         await _controller?.seekTo(Duration(milliseconds: 12300));
            //       },
            //     ),
            //     RaisedButton(
            //       child: Text('play'),
            //       onPressed: () async {
            //         await _controller?.play();
            //       },
            //     ),
            //     RaisedButton(
            //       child: Text('pause'),
            //       onPressed: () async {
            //         await _controller?.pause();
            //       },
            //     ),
            //     RaisedButton(
            //       child: Text('mute'),
            //       onPressed: () async {
            //         _controller?.mute();
            //       },
            //     ),
            //     RaisedButton(
            //       child: Text('unmute'),
            //       onPressed: () async {
            //         _controller?.unMute();
            //       },
            //     ),
            //     RaisedButton(
            //       child: Text('reload'),
            //       onPressed: () async {
            //         await _controller?.initialize();
            //       },
            //     ),
            //     RaisedButton(
            //       child: Text('push'),
            //       onPressed: () async {
            //         Navigator.push(context, MaterialPageRoute(builder: (ctx) => YoutubePlayerDemo()));
            //       },
            //     ),
            //   ],
            // ),
          ),
        ));
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class OriginYoutubePlayer extends StatefulWidget {
  final String title;
  final String youtubeUrl;
  int startPoint;
  int endPoint;
  final String? thumbnailUrl;
  final int? videoIdx;
  final bool? fullscreenEnable;
  final bool? isFullScreen;

  OriginYoutubePlayer(
      {Key? key,
      this.title = '',
      this.youtubeUrl = '',
      this.startPoint = 0,
      this.endPoint = 0,
      this.thumbnailUrl,
      this.fullscreenEnable = false,
      this.isFullScreen = false,
      this.videoIdx})
      : super(key: key);

  @override
  OriginYoutubePlayerState createState() => OriginYoutubePlayerState();
}

class OriginYoutubePlayerState extends State<OriginYoutubePlayer>
    with WidgetsBindingObserver, RouteAware, AutomaticKeepAliveClientMixin {
  YoutubePlayerController? _controller;
  bool isCurrentPage = true;

  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    initVideoPlayer(withAutoPlay: false);
    super.initState();
  }

  void initVideoPlayer({bool withAutoPlay = true}) async {
    String url = '${widget.youtubeUrl}';

    url = url.replaceAll("https://youtu.be/", "");
    url = url.replaceAll("URL: ", "");
    url = url.replaceAll("https://www.youtube.com/watch?v=", "");

    Duration startDuration = Duration(milliseconds: widget.startPoint);
    Duration endDuration = Duration(milliseconds: widget.endPoint);

    _controller = YoutubePlayerController(
        initialVideoId: 'https://youtu.be/zjLqYgxK39k',
        onReady: () {
          debugPrint('onReady');
          playPlayers();
        },
        onError: (value) {
          debugPrint('onError');
        },
        params: YoutubePlayerParams(
          autoPlay: withAutoPlay,
          desktopMode: false,
          showVideoAnnotations: false,
          showFullscreenButton: false,
          showControls: false,
          enableCaption: false,
          privacyEnhanced: true,
          startAt: startDuration,
          endAt: endDuration,
        ));
  }

  @override
  void dispose() {
    disposePlayers();
    debugPrint('disposed Player!!!');
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    debugPrint('AppLifecycleState state : ${state} : ${isCurrentPage}');
    if (state == AppLifecycleState.inactive) {
      // if (isCurrentPage) {
      //   AudioSession.instance.then((session) {
      //     session.configure(const AudioSessionConfiguration(
      //         avAudioSessionMode: AVAudioSessionMode.defaultMode
      //     ));
      //     session.setActive(true);
      //   });
      // }
      _controller?.pause();
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didPushNext() {
    isCurrentPage = false;
    _controller?.pause();
  }

  @override
  void didPopNext() {
    isCurrentPage = true;
  }

  void playPlayers() async {
    try {
      if (mounted && isCurrentPage) {
        _controller?.play();
      }
    } catch (e) {
      debugPrint('playPlayers error = ${e}');
    }
  }

  void pausePlayers() async {
    _controller?.pause();
  }

  void disposePlayers({bool withDispose = false}) async {
    _controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    Widget noramlPlayer = YoutubePlayerControllerProvider(
      controller: _controller!,
      child: Stack(
        children: [
          AbsorbPointer(
              absorbing: true,
              child: DubYoutubePlayer(
                controller: _controller!,
                aspectRatio: 16 / 9,
              )),
        ],
      ),
    );

    return Container(
      color: Colors.white,
      child: Column(
        children: [
          noramlPlayer,
          RaisedButton(onPressed: () async {
            await _controller?.pause();
            bool? hasError = await _controller?.seekTo(Duration(milliseconds: 100));
            await _controller?.play();
            await Future.delayed(Duration(milliseconds: 800));
            await _controller?.pause();
          }),
          RaisedButton(onPressed: () async {
            await _controller?.pause();
            await _controller?.seekTo(Duration(milliseconds: 1100));
            await _controller?.play();
            await Future.delayed(Duration(milliseconds: 800));
            await _controller?.pause();
          }),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
