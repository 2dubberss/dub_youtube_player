// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:io';

import 'package:audio_session/audio_session.dart';
import 'package:dub_youtube_player/dub_youtube_player.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rxdart/rxdart.dart';
import 'enums/player_state.dart';
import 'enums/playlist_type.dart';
import 'enums/thumbnail_quality.dart';
import 'enums/youtube_error.dart';
import 'meta_data.dart';
import 'player_params.dart';
import 'player_value.dart';
import 'dart:math' as math;

typedef ErrorHandler = void Function(YoutubeError error);

class YoutubePlayerController extends Stream<YoutubePlayerValue> implements Sink<YoutubePlayerValue> {
  /// Creates [YoutubePlayerController].
  YoutubePlayerController({
    required this.initialVideoId,
    this.params = const YoutubePlayerParams(),
    this.onReady,
    this.onError,
    this.isMute = false,
  }) {
    invokeJavascript = (_) async {};
  }

  /// The Youtube video id for initial video to be loaded.
  final String initialVideoId;

  final bool? isMute;

  final VoidCallback? onReady;

  final ErrorHandler? onError;

  /// Defines default parameters for the player.
  final YoutubePlayerParams params;

  /// Can be used to invokes javascript function.
  ///
  /// Ensure that the player is ready before using this.
  late Future<void> Function(String function) invokeJavascript;

  /// Called when player enters fullscreen.
  VoidCallback? onEnterFullscreen;

  /// Called when player exits fullscreen.
  VoidCallback? onExitFullscreen;

  final StreamController<YoutubePlayerValue> _controller = StreamController.broadcast();
  final PublishSubject<PlayerState> _playerStateStream = PublishSubject<PlayerState>();
  YoutubePlayerValue _value = YoutubePlayerValue();

  Stream<PlayerState> get playerStateStream => _playerStateStream.distinct();

  /// The [YoutubePlayerValue].
  YoutubePlayerValue get value => _value;

  Future<bool> initialize() async {
    String url = initialVideoId;

    url = url.replaceAll("https://youtu.be/", "");
    url = url.replaceAll("URL: ", "");
    url = url.replaceAll("https://www.youtube.com/watch?v=", "");

    Duration startDuration = params.startAt;
    Duration endDuration = params.endAt ?? Duration.zero;

    invokeJavascript('''
    player.loadVideoById({videoId:"${url}",startSeconds:"${startDuration.inMilliseconds / 1000}",endSeconds:${endDuration.inMilliseconds / 1000}});
    ''');
    // WAIT FOR VIDEO LOADED
    await (this).every((event) {
      if (value.hasError) {
        return false;
      }
      if (event.metaData.title.isNotEmpty) {
        return false;
      } else {
        return true;
      }
    }).timeout(const Duration(seconds: 5), onTimeout: () {
      return true;
    }).onError((error, stackTrace) {
      return true;
    });
    mute();

    await play();
    if ((isMute ?? false)) {
      mute();
    } else {
      unMute();
    }
    await pause();

    if (params.autoPlay) {
      play();
    }
    if (value.hasError) {
      onError?.call(value.error);
      return false;
    } else {
      onReady?.call();
      return true;
    }
  }

  Future<bool> loadVideo({String? videoId, Duration? startDuration, Duration? endDuration}) async {
    String url = videoId ?? '';

    url = url.replaceAll("https://youtu.be/", "");
    url = url.replaceAll("URL: ", "");
    url = url.replaceAll("https://www.youtube.com/watch?v=", "");

    Duration start = startDuration ?? Duration.zero;
    Duration end = endDuration ?? Duration.zero;

    invokeJavascript('''
    player.loadVideoById({videoId:"${url}",startSeconds:"${start.inMilliseconds / 1000}",endSeconds:${end.inMilliseconds / 1000}});
    ''');

    await play();
    await pause();
    _updateId(url);
    return true;
  }

  @override
  void add(YoutubePlayerValue data) {

    if (value.hasError) {
      return;
    }

    if (data.playerState == PlayerState.buffering) {
      hideYoutubeOverlay();
    }
    _value = data;
    _controller.add(_value);
    addStateChanged(data.playerState);
  }

  void addStateChanged(PlayerState state) {
    _playerStateStream.add(state);
  }

  @override
  StreamSubscription<YoutubePlayerValue> listen(
    void Function(YoutubePlayerValue event)? onData, {
    Function? onError,
    void Function()? onDone,
    bool? cancelOnError,
  }) {
    return _controller.stream.listen(
      (value) {
        _value = value;
        onData?.call(value);
      },
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    );
  }

  @override
  Future<void> close() => _controller.close();

  void stop() => invokeJavascript('stop()');

  void nextVideo() => invokeJavascript('next()');

  void previousVideo() => invokeJavascript('previous()');

  void playVideoAt(int index) => invokeJavascript('playVideoAt($index)');

  Future<bool> isVideoIdEmpty() async {
    if (value.hasError) {
      return false;
    }

    bool isIdEmpty = await (this).every((event) {
      if (value.hasError) {
        return false;
      }
      if (event.metaData.videoId.isNotEmpty) {
        return false;
      } else {
        return true;
      }
    }).timeout(const Duration(seconds: 5), onTimeout: () {
      return true;
    }).onError((error, stackTrace) {
      return true;
    });
    return isIdEmpty;
  }

  void _updateId(String id) {
    if (id.length != 11) {
      add(_value.copyWith(error: YoutubeError.invalidParam));
    } else {
      add(_value.copyWith(error: YoutubeError.none, hasPlayed: false));
    }
  }

  void mute() => invokeJavascript('mute()');

  void unMute() => invokeJavascript('unMute()');

  void setVolume(int volume) => volume >= 0 && volume <= 100
      ? invokeJavascript('setVolume($volume)')
      : throw Exception("Volume should be between 0 and 100");

  Future<bool> seekTo(Duration seekDuration, {bool allowSeekAhead = true}) async {
    double seekTo = 0.0;

    if (Platform.isIOS) {
      seekTo = math.max(0.0, seekDuration.inMilliseconds - 200) / 1000;
      invokeJavascript('seekTo(${seekTo},$allowSeekAhead)');
    } else {
      seekTo = seekDuration.inMilliseconds / 1000;
      invokeJavascript('seekTo(${seekTo},$allowSeekAhead)');
    }
    bool hasError = await (this).every((event) {
      if ((event.position.inMilliseconds / 1000) >= seekTo) {
        return false;
      } else {
        return true;
      }
    }).timeout(const Duration(seconds: 5), onTimeout: () {
      return true;
    }).onError((error, stackTrace) {
      return true;
    });
    return hasError;
  }

  Future<bool> play() async {
    if (value.hasError) {
      return false;
    }

    invokeJavascript('play()');
    bool hasError = await (this).every((event) {
      if (value.hasError) {
        return false;
      }
      if (event.playerState == PlayerState.playing) {
        return false;
      } else {
        return true;
      }
    }).timeout(const Duration(seconds: 5), onTimeout: () {
      return true;
    }).onError((error, stackTrace) {
      return true;
    });
    return hasError;
  }

  Future<bool> pause() async {
    if (value.hasError) {
      return false;
    }

    invokeJavascript('pause()');
    bool hasError = await (this).every((event) {
      if (value.hasError) {
        return false;
      }
      if (event.playerState == PlayerState.paused) {
        return false;
      } else {
        return true;
      }
    }).timeout(const Duration(seconds: 5), onTimeout: () {
      return true;
    }).onError((error, stackTrace) {
      return true;
    });
    return hasError;
  }

  void setSize(Size size) => invokeJavascript('setSize(${size.width}, ${size.height})');

  void setPlaybackRate(double rate) => invokeJavascript('setPlaybackRate($rate)');

  void setLoop(bool loop) => invokeJavascript('setLoop($loop)');

  void setShuffle(bool shuffle) => invokeJavascript('setShuffle($shuffle)');

  void hideYoutubeOverlay() {
    invokeJavascript('hideGesture()');
    invokeJavascript('hidePopUp()');
    invokeJavascript('hideOverlay()');
    invokeJavascript('hideTop()');
  }

  YoutubeMetaData get metadata => _value.metaData;

  void dispose() {
    pause();
    close();
    _playerStateStream.close();
  }

  void reset() => add(
        _value.copyWith(
          isReady: false,
          isFullScreen: false,
          playerState: PlayerState.unknown,
          hasPlayed: false,
          position: Duration.zero,
          buffered: 0.0,
          error: YoutubeError.none,
          metaData: const YoutubeMetaData(),
        ),
      );

  static String? convertUrlToId(String url, {bool trimWhitespaces = true}) {
    if (!url.contains("http") && (url.length == 11)) return url;
    if (trimWhitespaces) url = url.trim();

    for (var regex in [
      r'^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$',
      r'^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$',
      r'^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$',
    ]) {
      Match? match = RegExp(regex).firstMatch(url);
      if (match != null && match.groupCount >= 1) return match.group(1);
    }

    return null;
  }

  static String getThumbnail({
    required String videoId,
    String quality = ThumbnailQuality.standard,
    bool webp = true,
  }) {
    return webp
        ? 'https://i3.ytimg.com/vi_webp/$videoId/$quality.webp'
        : 'https://i3.ytimg.com/vi/$videoId/$quality.jpg';
  }
}
